
sudo mkdir /var/www/iot_kattakada
pwd
sudo cp -r . /var/www/iot_kattakada/
cd /var/www/iot_kattakada

sudo apt install python3-venv -y
python3 -m venv venv
source venv/bin/activate


ls -ld
sudo chown -R $USER:www-data .
ls -ld

touch .env
echo "
FLASK_APP=wsgi.py
FLASK_ENV=production
" | sudo tee .env

pip install flask gunicorn

flask run --host '0.0.0.0'
gunicorn --workers 4 --bind 0.0.0.0:5000 wsgi:app

sudo apt install nginx
sudo systemctl enable nginx
sudo systemctl start nginx
sudo systemctl status nginx

sudo touch /etc/systemd/system/iot_kattakada.service
# Create iot_kattakada.service configuration
echo "
[Unit]
Description=iot_kattakada service
After=network.target

[Service]
User=$USER
Group=www-data
Environment="PATH=$PWD/venv/bin"
WorkingDirectory=$PWD
ExecStart=$PWD/venv/bin/gunicorn --workers 3 --bind unix:$PWD/iot_kattakada.sock wsgi:app

[Install]
WantedBy=multi-user.target
" | sudo tee /etc/systemd/system/iot_kattakada.service


sudo systemctl enable iot_kattakada
sudo systemctl start iot_kattakada
sudo systemctl status iot_kattakada


sudo touch /etc/nginx/sites-available/iot_kattakada.conf
# Create Nginx site configuration
echo "
server {
    listen 80;
    listen [::]:80;    server_name lk.openiot.in;

    location = /favicon.ico { access_log off; log_not_found off; }

    access_log /var/log/nginx/iot_kattakada.access.log;
    error_log /var/log/nginx/iot_kattakada.error.log;

    location / {
        include proxy_params;
        proxy_pass http://unix:/var/www/iot_kattakada/iot_kattakada.sock;
    }
}
" | sudo tee /etc/nginx/sites-available/iot_kattakada.conf

sudo ln -s /etc/nginx/sites-available/iot_kattakada.conf /etc/nginx/sites-enabled/

ls -l /etc/nginx/sites-enabled/ | grep iot_kattakada

sudo nginx -t
sudo systemctl restart nginx
sudo systemctl status nginx
sudo systemctl status iot_kattakada



