# Kattakkada IoT Dashboard
![DASHBOARD](Images/dashboard.png)
## Overview
This project is a static website powered by Flask, serving as a showcase for redirection URLs to IoT device dashboards established in Kattakkada. The website provides a simple and static interface for users to easily access and explore various IoT dashboards associated with devices deployed in the region.

## Key Features
- **Redirection URLs:** Direct links to specific IoT dashboards for seamless access.
- **Static Interface:** The website is static, offering straightforward navigation without dynamic content.
- **User-Friendly Design:** A clean and simple design for a hassle-free user experience.

## Purpose
The primary objective is to offer a centralized platform where users can conveniently view and navigate to different IoT dashboards. This enhances accessibility and promotes awareness of the IoT devices deployed in Kattakkada.

## Technologies Used
- **Flask Framework:** Utilized for serving static content and managing routes.
- **HTML and CSS:** Front-end technologies for crafting a clean and user-friendly interface.
- **Version Control:** Git for code management.

## Install Requirements

- [Flask](https://pypi.org/project/Flask/)
    ```bash
    pip install flask
    ```

## How to Access
1. Clone the repository:
    ```bash
    git clone https://gitlab.com/techworldthink/icfoss_lorawan_kattakada.git
    ```

2. Run the Flask application:
    ```bash
    cd icfoss_lorawan_kattakada
    flask run --host '0.0.0.0'
    ```

3. Access the showcase in your web browser:
    - Open your web browser and navigate to [http://127.0.0.1:5000](http://127.0.0.1:5000)

Feel free to adjust this template to accurately reflect the characteristics of your static Flask-based IoT dashboard showcase.


